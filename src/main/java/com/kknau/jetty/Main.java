package com.kknau.jetty;

import org.eclipse.jetty.jmx.MBeanContainer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.lang.management.ManagementFactory;

/**
 * Created by Kami-sama on 21.05.2014.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);

        MBeanContainer mbContainer = new MBeanContainer(ManagementFactory.getPlatformMBeanServer());
        server.addBean(mbContainer);

        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/em");
//        webapp.setResourceBase("src/main/webapp");
        webapp.setResourceBase("../src/main/webapp");
        webapp.setParentLoaderPriority(true);
        server.setHandler(webapp);

        server.start();
        server.join();
    }
}
