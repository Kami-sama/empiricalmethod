package com.kknau.em.service;

import Jama.Matrix;
import com.empiricalmethods.*;
import com.kknau.em.dto.KMeanResult;
import com.kknau.em.pcatransform.LDA;
import com.kknau.em.pcatransform.PCA;

import java.util.*;

public class Methods {
    public HashMap<String, Object> lab1() {
        List<Integer> integerList = new ArrayList<Integer>(Arrays.asList(12, 13, 64, 17, 13, 25, 43, 43, 9, 87));
        Double average = 0d;
        Double averageError = 0d;
        Double mean = 0d;

        for (Integer e : integerList) {
            average += e;
        }
        average = average / integerList.size();

        Collections.sort(integerList);


        if (integerList.size() % 2 != 0) {
            mean = Double.valueOf(integerList.get(integerList.size() / 2));
        } else {
            int e = Math.round(integerList.size() / 2);
            mean = Double.valueOf((integerList.get(e) + integerList.get(e - 1)) / 2);
        }

        averageError = mean / average;

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("average", average);
        map.put("averageError", averageError);
        map.put("mean", mean);
        map.put("list", integerList);

        return map;
    }

    public HashMap<String, Object> lab2() {
        List<Integer> list1 = new ArrayList<Integer>(Arrays.asList(12, 32, 14, 4, 56, 64, 43));
        List<Integer> list2 = new ArrayList<Integer>(Arrays.asList(32, 65, 73, 84, 5, 23, 91));

        double sum1 = 0, sum2 = 0;
        for (int i = 0; i < 7; i++) {
            sum1 += list1.get(i);
            sum2 += list2.get(i);
        }

        sum1 = sum1 / list1.size();
        sum2 = sum2 / list2.size();

        double moda = (sum1 + sum2) / 2;

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("list1", list1);
        map.put("list2", list2);
        map.put("moda", moda);
        return map;
    }

    public List lab3() {
        ThirdLab third = new ThirdLab();
        Integer[] data = {30, 22, 33, 34, 28, 29, 34, 45, 46, 35, 40, 43, 44};
        List<Integer> dataList = Arrays.asList(data);
        int length = dataList.size();
        int min = Collections.min(dataList);

        int stergessNum = third.stergessNum(length);
        int swipeSampling = third.swipeSampling(min, Collections.max(dataList));
        int groupInterval = third.groupInterval(swipeSampling, stergessNum);
        Map<String, Integer[]> interval = third.groupIntervalMap(min, groupInterval / 2, stergessNum);

        List list = new ArrayList();

        list.add("Коофециент Стреджесса " + stergessNum);
        list.add("Размах выборки " + swipeSampling);
        list.add("Величина интервала группировки " + groupInterval);
        list.add("Половина классового интервала " + groupInterval / 2);
        list.add("Массив средин классовых интервалов " + Arrays.toString(interval.get("data")));
        list.add("Массивы: \n " + Arrays.toString(third.groupLists(dataList, interval.get("data"), groupInterval / 2, interval.get("output"))));

        return list;
    }

    public HashMap<String, Object> lab4() {
        FourLab fourLab = new FourLab();
        fourLab.average();
        fourLab.calcStandardDeviation();

        List list = new ArrayList();
        list.add("Среднее значение массива #1 " + fourLab.fAverage);
        list.add("Среднее значение массива #2 " + fourLab.sAverage);
        list.add("Стандартное отклонение массива #1 " + fourLab.fStandardDeviation);
        list.add("Стандартное отклонение массива #2 " + fourLab.sStandardDeviation);
        list.add("Размер массива #1 " + fourLab.getFirstList().size());
        list.add("Размер массива #2 " + fourLab.getSecondList().size());
        list.add("t-критерий Стьюдента :" + fourLab.studentValue());
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("list1", list);
        map.put("list2", fourLab.hotelling());
        return map;
    }

    public List lab5() {
        FifthLab fifthLab = new FifthLab();

        List list = new ArrayList();
        list.add("Массив 1-й выборки " + fifthLab.getFirstList());
        list.add("Массив 2-й выборки " + fifthLab.getSecondList());
        list.add("Минимальное значение величины W " + fifthLab.minValue() + "%");
        list.add("Максимальное значение величины W " + fifthLab.maxValue() + "%");
        list.add("U-критерий 1-й выборки " + fifthLab.firstUValue());
        list.add("U-критерий 2-й выборки " + fifthLab.secondUValue());
        return list;
    }

    public List lab6() {
        SixLab sixLab = new SixLab();
        List list = new ArrayList();
        list.add("Массив #1 " + sixLab.getFirstList());
        list.add("Массив #2 " + sixLab.getSecondList());
        list.add("Массив #3 " + sixLab.getThirdList());
        list.add("Многофакторный дисперсионный анализ для несвязанных выборок " + sixLab.anova());
        return list;
    }

    public List lab7() {
        SevenLab sevenLab = new SevenLab();
        List list = new ArrayList();
        list.add("Массив #1 " + sevenLab.getDataList());
        list.add("Коэффициент корреляции знаков Фехнера " + sevenLab.fehnerNumber());
        list.add("Коэффициент ранговой корреляции Кендалла " + sevenLab.kendal());
        return list;
    }

    public Matrix lab8() {
        Matrix sourceMatrix = new Matrix(new double[][]{
                {11, 22, 13, 14, 15, 18},
                {16, 15, 14, 19, 20, 21},
                {20, 20, 20, 20, 12, 22}});
        PCA pca = new PCA(sourceMatrix);
        Matrix valueMatrix = new Matrix(new double[][]{
                {2, 1, 2, 1, 1, 1},
                {1, 2, 1, 1, 2, 2}});
        Matrix transformedMatrix = pca.transform(valueMatrix, PCA.TransformationType.WHITENING);
        System.out.println("Трансформированная матрица:");
        for (int r = 0; r < transformedMatrix.getRowDimension(); r++) {
            for (int c = 0; c < transformedMatrix.getColumnDimension(); c++) {
                System.out.print(transformedMatrix.get(r, c));
                if (c == transformedMatrix.getColumnDimension() - 1) {
                    continue;
                }
                System.out.print(", ");
            }
            System.out.println("");
        }
        return transformedMatrix;
    }

    public KMeanResult lab9() {
        return (new KMeans()).getData();
    }

    public String lab10() {
        int[] group = {1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
        double[][] sourceValues = {{2.95, 6.63}, {2.53, 7.79}, {3.57, 5.65}, {3.16, 5.47},
                {2.58, 4.46}, {2.16, 6.22}, {3.27, 3.52}, {3.88, 3.91},
                {3.98, 4.01}, {4.18, 4.21}};

        LDA lda = new LDA(sourceValues, group, true);
        double[] values = lda.getDiscriminantFunctionValues(new double[]{2.81, 5.46, 8.11});
        for (int i = 0; i < values.length; i++) {
            System.out.println("Дискрименантные значения " + (i + 1) + ": " + values[i]);
        }

        return Arrays.toString(values);
    }
}