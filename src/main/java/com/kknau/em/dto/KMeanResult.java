package com.kknau.em.dto;

import java.util.ArrayList;

/**
 * Created by Kami-sama on 08.06.2014.
 */
public class KMeanResult {
    private int NUM_CLUSTERS;
    private int TOTAL_DATA;
    private ArrayList dataSet;
    private ArrayList centroids;

    @Override
    public String toString() {
        return "KMeanResult{" +
                "NUM_CLUSTERS=" + NUM_CLUSTERS +
                ", TOTAL_DATA=" + TOTAL_DATA +
                ", dataSet=" + dataSet +
                ", centroids=" + centroids +
                '}';
    }

    public int getNUM_CLUSTERS() {
        return NUM_CLUSTERS;
    }

    public void setNUM_CLUSTERS(int NUM_CLUSTERS) {
        this.NUM_CLUSTERS = NUM_CLUSTERS;
    }

    public int getTOTAL_DATA() {
        return TOTAL_DATA;
    }

    public void setTOTAL_DATA(int TOTAL_DATA) {
        this.TOTAL_DATA = TOTAL_DATA;
    }

    public ArrayList getDataSet() {
        return dataSet;
    }

    public void setDataSet(ArrayList dataSet) {
        this.dataSet = dataSet;
    }

    public ArrayList getCentroids() {
        return centroids;
    }

    public void setCentroids(ArrayList centroids) {
        this.centroids = centroids;
    }
}
