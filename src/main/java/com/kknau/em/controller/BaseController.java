package com.kknau.em.controller;

import com.kknau.em.service.Methods;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

public class BaseController extends HttpServlet {
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        Methods methods = new Methods();
        req.setAttribute("methods", methods);
        req.getRequestDispatcher("methods.jsp").forward(req, res);
    }
}
