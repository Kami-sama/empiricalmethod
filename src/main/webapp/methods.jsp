<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Hi ANya Nikolai</title>
</head>
<body>
<style>
    div {
        background-color: beige;
    }
</style>
<a href="/em">На главную</a>

<ul>
    <li><a href="#lab1">Lab 1</a></li>
    <li><a href="#lab2">Lab 2</a></li>
    <li><a href="#lab3">Lab 3</a></li>
    <li><a href="#lab4">Lab 4</a></li>
    <li><a href="#lab5">Lab 5</a></li>
    <li><a href="#lab6">Lab 6</a></li>
    <li><a href="#lab7">Lab 7</a></li>
    <li><a href="#lab8">Lab 8</a></li>
    <li><a href="#lab9">Lab 9</a></li>
    <li><a href="#lab10">Lab 10</a></li>
</ul>


<div id="lab1" name="lab1">
    <h3>Lab 1</h3>
    Массив = ${methods.lab1().get("list")} <br/>
    Среднее значение = ${methods.lab1().get("average")} <br/>
    Центральное значение = ${methods.lab1().get("mean")} <br/>
    Ошибка среднего = ${methods.lab1().get("averageError")} <br/>
</div>

<div id="lab2" name="lab2">
    <h3>Lab 2</h3>
    Массив #1 = ${methods.lab2().get("list1")} <br/>
    Массив #2 = ${methods.lab2().get("list2")} <br/>
    Мода = ${methods.lab2().get("moda")} <br/>
</div>

<div id="lab3" name="lab3">
    <h3>Lab 3</h3>
    <c:forEach items="${methods.lab3()}" var="i">
        <c:out value="${i}"/><br/>
    </c:forEach>
</div>

<div id="lab4" name="lab4">
    <h3>Lab 4</h3>
    <c:forEach items="${methods.lab4().get('list1')}" var="i">
        <c:out value="${i}"/><br/>
    </c:forEach>
    <br/>

    <div style="margin-left: 100px;">
        <c:forEach items="${methods.lab4().get('list2')}" var="i">
            <c:out value="${i}"/><br/>
        </c:forEach>
    </div>
</div>

<div id="lab5" name="lab5">
    <h3>Lab 5</h3>
    <c:forEach items="${methods.lab5()}" var="i">
        <c:out value="${i}"/><br/>
    </c:forEach>
</div>

<div id="lab6" name="lab6">
    <h3>Lab 6</h3>
    <c:forEach items="${methods.lab6()}" var="i">
        <c:out value="${i}"/><br/>
    </c:forEach>
</div>

<div id="lab7" name="lab7">
    <h3>Lab 7</h3>
    <h4>Трансформированная матрица:</h4>
    <c:forEach items="${methods.lab7()}" var="i">
        <c:out value="${i}"/><br/>
    </c:forEach>
</div>

<div id="lab8" name="lab8">
    <h3>Lab 8</h3>
    <h4>Трансформированная матрица</h4>
    <c:set var="matrix" value="${methods.lab8()}"/>
    <c:forEach var="i" begin="0" end="${matrix.getRowDimension() - 1}">
        <c:forEach var="j" begin="0" end="${matrix.getColumnDimension() - 1}">
            <c:out value="${matrix.get(i,j)}"/>&nbsp;|&nbsp;
        </c:forEach>
        <br/>
    </c:forEach>
</div>

<div id="lab9" name="lab9">
    <h3>Lab 9</h3>
    <c:set value="${methods.lab9()}" var="result"/>
    <h4>Дата кластеров:</h4>
    <c:forEach items="${result.dataSet}" var="data">
        <c:out value="${data}"/><br/>
    </c:forEach>

    <h4>Центр масс:</h4>
    <c:forEach items="${result.centroids}" var="centroid">
        <c:out value="${centroid}"/><br/>
    </c:forEach>
    <%--<c:forEach items="result."></c:forEach>--%>
</div>

<div id="lab10" name="lab10">
    <h3>Lab 10</h3>
    <h4>Дискрименантные значения:</h4>
    <c:out value="${methods.lab10()}"/>
    <%--<c:set value="${methods.lab10()}" var="value"/>--%>
    <%--<c:forEach var="r" begin="0" end="${methods.lab10().length}">--%>
    <%--<c:out value="${r+1}) "/>--%>
    <%--&lt;%&ndash;<c:out value="${value[r]}"/>&ndash;%&gt;--%>
    <%--</c:forEach>--%>

</div>


</body>
</html>
